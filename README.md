In jenkins configuration set JDK and Maven

Maven Home : /usr/share/maven Name: MVN
JDK Home : /usr/local/openjdk-8/ Name: Java


///////// Logstash ///////////

settings (logstash.yml and jvm.options): /usr/share/logstash/config
conf (logstash pipeline configuration files): /usr/share/logstash/pipeline



//////// File Beat ///////////

Service file: /lib/systemd/system/filebeat.service
Configuration file: /etc/filebeat/filebeat.yml

Place 'filebeat.yml' in the above mentioned configuration path on your node where the logs should be shipped from.
